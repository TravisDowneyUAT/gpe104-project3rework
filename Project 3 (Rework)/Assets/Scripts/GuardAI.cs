﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuardAI : Controller
{
    public int health = 4;
    public Image healthBar;

    private Transform playerTransform;

    //FSM variables
    private Animator animator;
    bool chasing = false;
    bool waiting = false;
    private float distanceFromTarget;
    public bool inFlashLight;

    //Where is the guard going and how fast?
    Vector3 direction;
    private float walkSpeed = 2.0f;
    private int currentTarget;
    private Transform[] waypoints = null;

    private void Awake()
    {
        //Get a reference to the player's transform
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        //Get reference to the FSM
        animator = gameObject.GetComponent<Animator>();

        //Add all waypoints to the waypoints array
        Transform point1 = GameObject.Find("p1").transform;
        Transform point2 = GameObject.Find("p2").transform;
        Transform point3 = GameObject.Find("p3").transform;
        Transform point4 = GameObject.Find("p4").transform;
        waypoints = new Transform[4]
        {
            point1,
            point2,
            point3,
            point4
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (chasing)
        {
            direction = playerTransform.position - transform.position;
            rotateGuard();
        }

        if (!waiting)
        {
            transform.Translate(walkSpeed * direction * Time.deltaTime, Space.World);
        }
    }

    private void FixedUpdate()
    {
        //Give the values to the FSM
        distanceFromTarget = Vector3.Distance(waypoints[currentTarget].position, transform.position);
        animator.SetFloat("distanceFromWaypoint", distanceFromTarget);
        animator.SetBool("playerInSight", inFlashLight);
    }

    public void SetNextPoint()
    {
        int nextPoint = -1;

        do
        {
            nextPoint = Random.Range(0, waypoints.Length - 1);
        } while (nextPoint == currentTarget);

        currentTarget = nextPoint;

        //Load the direction of the next waypoint
        direction = waypoints[currentTarget].position - transform.position;
        rotateGuard();
    }

    public void Chase()
    {
        //Load the direction of the player
        direction = playerTransform.position - transform.position;
        rotateGuard();
    }

    public void StopChasing()
    {
        chasing = false;
    }

    private void rotateGuard()
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        direction = direction.normalized;
    }

    public void StartChasing()
    {
        chasing = true;
    }

    public void ToggleWaiting()
    {
        waiting = !waiting;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        healthBar.fillAmount = health / 4f;

        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
