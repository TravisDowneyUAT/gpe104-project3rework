﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public float speed = 20.0f;
    public int damage = 1;
    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        GuardAI guard = hitInfo.GetComponent<GuardAI>();
        if (guard != null)
        {
            guard.TakeDamage(damage);
        }

        Destroy(gameObject);
    }
}
