﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    private Vector3 target;
    public GameObject crosshairs;
    public GameObject player;
    public GameObject knifePrefab;
    public GameObject knifeHand;
    public Transform knifehand;

    public float knifeSpeed = 60.0f;
    private int damage = 1;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        target = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        crosshairs.transform.position = new Vector2(target.x, target.y);

        Vector3 difference = target - player.transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        player.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            float distance = difference.magnitude;
            Vector2 direction = difference / distance;
            direction.Normalize();
            ThrowKnife(direction, rotationZ);
        }
    }

    void ThrowKnife(Vector2 direction, float rotationZ)
    {
        GameObject k = Instantiate(knifePrefab) as GameObject;
        k.transform.position = knifehand.transform.position;
        k.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);
        k.GetComponent<Rigidbody2D>().velocity = direction * knifeSpeed;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        GuardAI guard = collision.GetComponent<GuardAI>();
        if (guard != null)
        {
            guard.TakeDamage(damage);
        }
        Destroy(gameObject);
    }
}
