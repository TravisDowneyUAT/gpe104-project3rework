﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject document; //game object variable to hold the document prefab
    public GameObject exit; //game object variable to hold the exit door
    public GameObject player; //game object variable to hold the player

    private int lives; //int variable to hold the amount of player lives
    public int itemsRemaining = 4; //in variable to hold the amount of items remaining

    public Text livesText; //text variable to hold the lives text in the canvas
    public Text itemsText; //text variable to hold the items text in the canvas



    // Start is called before the first frame update
    void Start()
    {
        player.GetComponent<PlayerController>();
        BeginGame();
    }

    // Update is called once per frame
    void Update()
    {
        //quits out of the game if the player presses the escape key
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    //begins the game and sets up the HUD for the player
    void BeginGame()
    {
        player.transform.position = new Vector3(-8.3f, 0.2f, 0.0f); //spawns the player back at the start of the level
        player.GetComponent<PlayerController>().healthBar.fillAmount = 1f / 1f; //refills the player health bar

        lives = 1;
        itemsRemaining = 4;

        //HUD
        livesText.text = "Lives: " + lives;
        itemsText.text = "Items: " + itemsRemaining;

        SpawnItems();
    }

    //spawns in the collectible items throughout the level
    void SpawnItems()
    {
        DestroyExistingItems();

        if (itemsRemaining == 4)
        {
            Instantiate(document, new Vector3(-4.1f, -2.3f, 0.0f), Quaternion.identity);

            Instantiate(document, new Vector3(-0.1f, 2.1f, 0.0f), Quaternion.identity);

            Instantiate(document, new Vector3(4.5f, -2.3f, 0.0f), Quaternion.identity);

            Instantiate(document, new Vector3(6.2f, 2.0f, 0.0f), Quaternion.identity);
        }
    }

    //decrements the items as the player collects them
    public void DecrementItems()
    {
        itemsRemaining--;

        itemsText.text = "Items: " + itemsRemaining;
        
        if(itemsRemaining <= 0)
        {
            exit.GetComponent<ExitTrigger>().enabled = true;
        }
    }

    //decrements the players lives as the players health is reduced to zero
    public void DecrementLives()
    {
        lives--;
        livesText.text = "Lives: " + lives;

        //restarts the game if the players lives drop below one
        if (lives < 1)
        {
            BeginGame();
        }
    }
    
    // destroys any items left over from previous playthrough
    void DestroyExistingItems()
    {
        GameObject[] documents =
            GameObject.FindGameObjectsWithTag("Item");

        foreach(GameObject current in documents)
        {
            GameObject.Destroy(current);
        }
    }
}
