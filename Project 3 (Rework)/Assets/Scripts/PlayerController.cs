﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : Controller
{
    private float speed = 1.5f; //float variable for player speed
    private float playerHealth = 1; //float variable for player health
    private int damage = 1; // float variable for the damage the player takes
    public Image healthBar; //image variable to display the players health bar and fill it whenever they take damage

    private GameManager gameManager; //game manager variable to interact with the game manager

    // Start is called before the first frame update
    void Start()
    {
        //calls the GameManager script from the MainCamera
        GameObject gameManagerObject =
            GameObject.FindWithTag("MainCamera");
        gameManager = gameManagerObject.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //gets all the key input for player movement
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
    }

    //Triggers the following if statements depending on what trigger collider the player
    //runs into
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //is triggered if the players wanders into the guards flashlight
        if(collision.gameObject.tag.Equals("Light"))
        {
            ReduceHealth(damage);
        }
        //is triggered if the player walks over an item
        if (collision.gameObject.tag.Equals("Item"))
        {
            gameManager.DecrementItems();            
        }
        //is triggered if the player collects all the items and makes it to the exit
        if (collision.gameObject.tag.Equals("Exit") && gameManager.itemsRemaining == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    //runs when the player is damaged
    void ReduceHealth(int damage)
    {
        playerHealth -= damage;

        healthBar.fillAmount = playerHealth / 1f;
        
        //tells the gamemanager script to decrement the player's lives
        //and tells the game to destroy the player on their health reaches zero
        if(playerHealth <= 0)
        {
            gameManager.DecrementLives();
        }
    }
}
